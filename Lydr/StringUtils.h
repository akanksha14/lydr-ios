//
//  StringUtil.h
//  InstaPat
//
//  Created by Akanksha Sharma on 3/18/15.
//  Copyright (c) 2015 com.charan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject

-(NSString *) convertBrToNewlines: (NSString*) str;

-(NSString *) hash :(NSString*) str;


@end
