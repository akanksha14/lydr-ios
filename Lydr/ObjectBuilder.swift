//
//  ObjectBuilder.swift
//  InstaPat
//
//  Created by Akanksha Sharma on 5/22/15.
//  Copyright (c) 2015 kwench. All rights reserved.
//

import Foundation

class ObjectBuilder: NSObject {
    
    func objectsFromJSON(data : NSData,jsonName : NSString,error :  NSErrorPointer) -> NSDictionary{
        var jsonObject: NSDictionary = NSDictionary()
        print("objectsFromJSON begins")
        print("creating object \(data)")
        
        if(jsonName == zipCodeSearchReq){
            
            let arr : NSArray  = try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves) as! NSArray
            let zipCodesDict : NSDictionary = NSDictionary(object: arr, forKey: "zipCodes")
            return zipCodesDict

        } else {
            jsonObject = try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves) as! NSDictionary
            NSLog("%@",jsonObject)
            return jsonObject
        }

    }
}
