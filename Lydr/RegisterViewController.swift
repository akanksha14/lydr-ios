//
//  RegisterViewController.swift
//  Lydr
//
//  Created by Akanksha Sharma on 28/10/15.
//  Copyright © 2015 akanksha. All rights reserved.
//

import Foundation
import DLRadioButton

class RegisterViewController: UIViewController,NetworkManagerDelegate {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var aliasName: UITextField!
    @IBOutlet weak var maleRadioBtn: DLRadioButton!
    @IBOutlet weak var femaleRadioBtn: DLRadioButton!
    @IBOutlet weak var othersRadioBtn: DLRadioButton!

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var liveInCountryAmerica: DLRadioButton!
    @IBOutlet weak var liveInCountryIndia: DLRadioButton!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    
    @IBOutlet weak var zipCode: UITextField!
    @IBOutlet weak var ineterestedCountryIndia: DLRadioButton!
    @IBOutlet weak var interestedCountryAmerica: DLRadioButton!
    
    @IBOutlet weak var cityNAme: UILabel!
    @IBOutlet weak var activityIndicatior: UIActivityIndicatorView!
    var genderRadioBtnGroup: NSMutableArray = NSMutableArray()
    var liveInCountriesBtnGroup: NSMutableArray = NSMutableArray()

    
    @IBOutlet weak var scrollView: CustomScrollView!
    
    var accessToken = ""
    var currentFetch = "";
    var stringUtils :StringUtils = StringUtils()
    var networkManager :NetworkManager = NetworkManager()
    var urlRequest : NSMutableURLRequest = NSMutableURLRequest()
    var stringUtil : StringUtils = StringUtils()
    var viewControllerUtils : ViewControllerUtils = ViewControllerUtils()
    var validationViews : NSMutableArray = NSMutableArray()
    var selectedGender : NSMutableString = NSMutableString()
    var selectedLiveInCountry : Int = 0
    var interestedCountries : NSMutableArray = NSMutableArray()
    var DOBString : String = String()
    var registerParamDict : NSMutableDictionary = NSMutableDictionary();
    var authParamDict : NSMutableDictionary = NSMutableDictionary();

    
    
    override func viewDidLoad() {
        networkManager.delegate = self
        networkManager.communicator = NetworkCommunicator()
        networkManager.communicator?.delegate = networkManager
        genderRadioBtnGroup.addObject(femaleRadioBtn);
        genderRadioBtnGroup.addObject(othersRadioBtn);
        maleRadioBtn.otherButtons = genderRadioBtnGroup as [AnyObject];
        maleRadioBtn.selected = true;
        interestedCountryAmerica.selected = false;
        ineterestedCountryIndia.selected = false;
        liveInCountryIndia.selected = true;
        liveInCountriesBtnGroup.addObject(liveInCountryAmerica);
        liveInCountryIndia.otherButtons = liveInCountriesBtnGroup as [AnyObject];
        if((registerParamDict["name"]) != nil){
            name.text = registerParamDict["name"] as? String;
        }
        if((registerParamDict["imageUrl"]) != nil){
            userImg.image = getImageFromURL(registerParamDict["imageUrl"] as! String)
        }

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print(scrollView.contentOffset.y)
        if(scrollView.contentOffset.y >= 200){
            scrollView.setContentOffset(CGPointMake(0, 0.0), animated: true);
        }
        self.view.endEditing(true)

    }
    
    @IBAction func selectGender( sender: AnyObject) {
        if(maleRadioBtn.selected){
            selectedGender = "M";
        }
        if(femaleRadioBtn.selected){
            selectedGender = "F";
        }
        if(othersRadioBtn.selected){
            selectedGender = "O";
        }
        print(selectedGender)
    }
    
    @IBAction func submitRegistration(sender: AnyObject) {
        if(validateData()){
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            //let parametersDict:NSMutableDictionary = NSMutableDictionary()
            registerParamDict.setValue(name.text , forKey: "name");
            registerParamDict.setValue(aliasName.text , forKey: "aliasName");
            if(yearTextField.text != "" && monthTextField.text != "" && dateTextField != ""){
                DOBString = yearTextField.text! + "-" + monthTextField.text! + "-" + dateTextField.text!
                registerParamDict.setValue(DOBString, forKey: "dateOfBirth");
            }
            registerParamDict.setValue(zipCode.text, forKey: "zipCodeId");
            registerParamDict.setValue(selectedGender, forKey: "gender");
            registerParamDict.setValue(selectedLiveInCountry, forKey: "countryId");
            registerParamDict.setValue(interestedCountries, forKey: "interestedCountries");
            if(authParamDict["loginMode"] as! Int != SocialLoginTypes.EMAIL.rawValue){
                registerParamDict.setValue(true, forKey: "socialLoginInd");
            } else {
                registerParamDict.setValue(false, forKey: "socialLoginInd");
            }
            authParamDict.setValue(registerParamDict, forKey: "user");
            print(authParamDict)
            currentFetch = registerRequest;
            let jsonData : NSData = try! NSJSONSerialization.dataWithJSONObject(authParamDict, options: [])
            urlRequest = NSMutableURLRequest(URL: NSURL(string: HOST_IP + registerURL)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = jsonData
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            networkManager.sendRequest(urlRequest, jsonName: loginRequest)

        }
        
    }
    @IBAction func selectLiveInCountry(sender: AnyObject) {
        selectedLiveInCountry = (sender as! DLRadioButton).tag
        print(selectedLiveInCountry)

    }
    
    @IBAction func selectInterestedCountry(sender: AnyObject) {
        let clickedBtn : DLRadioButton = sender as! DLRadioButton
        print(clickedBtn.selected)

        if(clickedBtn.selected){
            if(interestedCountries.containsObject(clickedBtn.tag)){
                interestedCountries.removeObject((sender as! DLRadioButton).tag)
                clickedBtn.selected = false;
            } else {
                clickedBtn.selected = true;
                interestedCountries.addObject((sender as! DLRadioButton).tag)
            }
        } else {
            clickedBtn.selected = false;
            interestedCountries.removeObject((sender as! DLRadioButton).tag)
        }
        print(interestedCountries)
        
    }
    
    @IBAction func editingBegin(sender: AnyObject) {
        scrollView.setContentOffset(CGPointMake(0, 200.0), animated: true);
    }
    
    func validateData() -> Bool {
        var isValid : Bool = true
        if(name.text == ""){
            validationViews.addObject(name);
            isValid = false;
        }
        if(aliasName.text == ""){
            validationViews.addObject(aliasName);
            isValid = false;
        }
        if(zipCode.text == ""){
            validationViews.addObject(zipCode);
            isValid = false;
        }
        if(validationViews.count != 0){
            showValidationErrors();
        }
        return isValid;
    }
    
    func showValidationErrors() {
        for (var i = 0; i < validationViews.count; i++){
            viewControllerUtils.applyShakeValidation(validationViews.objectAtIndex(i) as! UIView);
        }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        if(liveInCountryIndia.selected){
            if(newLength <= 6){
                return true
            } else {
                return false
            }
        } else {
            if(newLength <= 5){
                return true
            } else {
                return false
            }
        }
    }
    @IBAction func zipCodeEditingEnd(sender: AnyObject) {
        let tf : UITextField = (sender as! UITextField)
        let currentCharacterCount = tf.text?.characters.count
        if(liveInCountryIndia.selected){
            if(currentCharacterCount == 6){
                fetchCityByZipCode(tf.text!);
            }
        } else {
            if(currentCharacterCount == 5){
                fetchCityByZipCode(tf.text!);
            }
        }

    }
    
    func fetchCityByZipCode (zipCode : String){
        //activityIndicatior.hidden = false;
        activityIndicatior.startAnimating()
        currentFetch = zipCodeSearchReq;
        let urlStr : NSString = NSString(format:"%@%@%@", HOST_IP,zipCodeSearchURL, self.zipCode.text!)
        let url: NSURL? = NSURL(string: urlStr as String)
        if(url != nil){
            let urlRequest : NSMutableURLRequest? = NSMutableURLRequest(URL: url!)
            networkManager.sendRequest(urlRequest!, jsonName: zipCodeSearchReq)
        }
    }
    
    func didReceiveResponse(responseDict : NSDictionary){
        
        if(currentFetch == registerRequest){
            if(responseDict["access_token"] != nil){
                self.performSegueWithIdentifier("followLeadersView", sender: self);
                NSUserDefaults.standardUserDefaults().setObject(responseDict["access_token"] as! String, forKey: accessTokenKey)
                NSUserDefaults.standardUserDefaults().synchronize()
            } else if(responseDict["message"] as! String == "Unique Alias is required")  {
                let alert = UIAlertController(title: "Registration Alert", message: responseDict["message"] as? String, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)

            }
            
        } else {
            NSLog("auth failure \(responseDict)");
            activityIndicatior.stopAnimating()
            cityNAme.hidden = false;
            cityNAme.text = responseDict["zipCodes"]?.objectAtIndex(0) as? String;
        }
    }
    
    func requestFailedWithError(error : NSError){
        print(error)
        
        
    }
    
    func didReceiveResponseCode(response : NSHTTPURLResponse){
        print(response)
        
        
        
    }

    
}
