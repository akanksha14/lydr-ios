//
//  Constants.swift
//  Lydr
//
//  Created by Akanksha Sharma on 16/10/15.
//  Copyright © 2015 akanksha. All rights reserved.
//

import Foundation

// host ip constant
//let HOST_IP = "https://api.myperks.in/v1"
let HOST_IP = "http://192.168.0.7:2000/v1"
//let HOST_IP = "https://test-api.myperks.in/v1"
// unique key which identifies a device
let KEY = "fcde38b55ad400d5fcb3ddd32eba66bef671d613"

let kGoogleClientId = "1054685287989-008i0dnqh3g8n1vq3hrfks5rvv11blj3.apps.googleusercontent.com";
let googleURLScheme = "com.abrolly.lydr";
let fbURLScheme = "fb1706906742875863";

enum SocialLoginTypes: Int	{
    case EMAIL = 1
    case GOOGLEPLUS = 2
    case FACEBOOK = 3
    case TWITTER = 4
}

//KEYS
let accessTokenKey = "access-token"
let userConfigKey = "user"
let companyConfigKey = "company_config"
let configKey = "config"

//Login Keys
let deviceIdKey = "deviceId"
let deviceNameKey = "deviceName"
let deviceVersionKey = "version"
let platformKey = "platform"
let emailKey = "email"
let passwordKey = "password"
let socialAccessTokenKey = "socialAccessToken"

//URLs
let authURL = "/auth"
let registerURL = "/register"
let userConfigURL = "/user"
let logoutURL = "/logout"
let userListURL = "/search/users?term="
let zipCodeSearchURL = "/register/zipcodes?term="

//Requests
let loginRequest = "login"
let userConfigRequest = "userConfigRequest"
let registerRequest = "registerRequest"
let logoutRequest = "logoutRequest"
let userSearchRequest = "userSearchRequest"
let appreciateSubmitRequest = "appreciateSubmitRequest"
let spotSubmitRequest = "spotSubmitRequest"
let zipCodeSearchReq = "zipCodeSearchReq"

