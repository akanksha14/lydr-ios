//
//  NetworkCommunicator.swift
//  InstaPat
//
//  Created by Akanksha Sharma on 5/22/15.
//  Copyright (c) 2015 kwench. All rights reserved.
//

import Foundation

class NetworkCommunicator: NSObject,NSURLConnectionDataDelegate,NSURLConnectionDelegate {
    var delegate: NetworkCommunicatorDelegate?
    var responseData : NSMutableData?
    var jsonName : NSString =  ""
    
    
    func fetchDataRequest (request : NSURLRequest,jsonName : NSString){
        // Creating NSOperationQueue to which the handler block is dispatched when the request completes or failed
        let queue: NSOperationQueue = NSOperationQueue()
        
        // Sending Asynchronous request using NSURLConnection
        /*NSURLConnection.sendAsynchronousRequest(request, queue: queue) { (response:NSURLResponse?, responseDate:NSData?, error:NSError?) -> Void in
            if error != nil
            {
                print(error!.description)
                //self.removeActivityIndicator()
            }
            else
            {
                //Converting data to String
                let responseStr:NSString = NSString(data:self.responseData!, encoding:NSUTF8StringEncoding)!
                let modifiedStr = self.stringByRemovingControlCharacters(responseStr)
                print(modifiedStr)
               // let inputData :NSData = modifiedStr.dataUsingEncoding(NSUTF8StringEncoding)!
            }
        }*/
        
       /* NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{(response:NSURLResponse!, responseData:NSData!, error: NSError!) -> Void in
            
            if error != nil
            {
                print(error.description)
                //self.removeActivityIndicator()
            }
            else
            {
                //Converting data to String
                var responseStr:NSString = NSString(data:responseData, encoding:NSUTF8StringEncoding)!
                var modifiedStr = self.stringByRemovingControlCharacters(responseStr)
                var inputData :NSData = modifiedStr.dataUsingEncoding(NSUTF8StringEncoding)!
            }
        })*/
        responseData = NSMutableData()
        self.jsonName = jsonName
        let connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
        print(connection);

     }
    

    func stringByRemovingControlCharacters(inputString  :NSString) -> NSString{
        let controlChars : NSCharacterSet = NSCharacterSet.controlCharacterSet()
        var range : NSRange = inputString.rangeOfCharacterFromSet(controlChars)
        if(range.location != NSNotFound){
            let mutable : NSMutableString = NSMutableString(string: inputString)
            while(range.location != NSNotFound){
                mutable.deleteCharactersInRange(range)
                range = mutable.rangeOfCharacterFromSet(controlChars)
            }
            return mutable
        }
        return inputString
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse)
    { //It says the response started coming
        NSLog("didReceiveResponse :: \(response.MIMEType)")
        let httpResponse :NSHTTPURLResponse = response as! NSHTTPURLResponse
        let code : Int = httpResponse.statusCode
        if(code != 200){
            self.delegate?.didReceiveResponseCode(httpResponse)
            //connection.cancel()
        }
        
    }
    
    func connection(connection: NSURLConnection, didReceiveData _data: NSData)
    { //This will be called again and again until you get the full response
        NSLog("didReceiveData")
        // Appending data
        self.responseData!.appendData(_data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        // This will be called when the data loading is finished i.e. there is no data left to be received and now you can process the data.
        NSLog("connectionDidFinishLoading")
        let responseStr:NSString = NSString(data:responseData!, encoding:NSUTF8StringEncoding)!
       // var modifiedStr = self.stringByRemovingControlCharacters(responseStr)
       // println(modifiedStr)
       // var inputData :NSData = modifiedStr.dataUsingEncoding(NSUTF8StringEncoding)!
        print(responseStr)
        let inputData :NSData = responseStr.dataUsingEncoding(NSUTF8StringEncoding)!
        self.delegate?.didReceiveResponse(inputData, jsonName: jsonName)
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError){
        self.delegate?.requestFailedWithError(error)
    }

}

protocol NetworkCommunicatorDelegate{
    /**
    Called when the response is received.
    */
    func didReceiveResponse(objectNotation : NSData, jsonName : NSString)
    
    /**
    Called when the error is received.
    */
    func requestFailedWithError(error : NSError)
    
    /**
    Handles the response according to the response code
    */
    func didReceiveResponseCode(response : NSHTTPURLResponse)
}