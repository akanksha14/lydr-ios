//
//  ViewController.swift
//  Lydr
//
//  Created by Akanksha Sharma on 20/09/15.
//  Copyright (c) 2015 akanksha. All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKLoginKit

class LoginViewController: UIViewController,FBSDKLoginButtonDelegate, GPPSignInDelegate,NetworkManagerDelegate{

    @IBOutlet weak var fbLoginBtn: FBSDKLoginButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginToAppBtn: UIButton!
    @IBOutlet var loginViewTopSpace: NSLayoutConstraint!
    @IBOutlet weak var gppSignInBtn: GPPSignInButton!
    @IBOutlet weak var twitterSignInBtn: TWTRLogInButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var loginToAppView: UIView!
    
    
    var signIn = GPPSignIn.sharedInstance();
    var accessToken = ""
    var currentFetch = "";
    var stringUtils :StringUtils = StringUtils()
    var networkManager :NetworkManager = NetworkManager()
    var urlRequest : NSMutableURLRequest = NSMutableURLRequest()
    var stringUtil : StringUtils = StringUtils()
    var viewControllerUtils : ViewControllerUtils = ViewControllerUtils()
    var loginMode : Int = 1;
    var socialAccessToken : String = String();
    let parametersDict:NSMutableDictionary = NSMutableDictionary()
    var email : String = String();
    var fbUserImage : String = String()
    var name : String = String();
    var profileImg : String = String()
    var registerDict : NSMutableDictionary = NSMutableDictionary();


    override func viewDidLoad() {
        super.viewDidLoad()
        loginMode = SocialLoginTypes.EMAIL.rawValue;
        networkManager.delegate = self
        networkManager.communicator = NetworkCommunicator()
        networkManager.communicator?.delegate = networkManager
        signIn.shouldFetchGooglePlusUser = true;
        signIn.clientID = kGoogleClientId;
        signIn.shouldFetchGoogleUserEmail = true;
        signIn.shouldFetchGoogleUserID = true;
        signIn.scopes = [kGTLAuthScopePlusLogin,kGTLAuthScopePlusUserinfoEmail,kGTLAuthScopePlusUserinfoProfile];
        fbLoginBtn.titleLabel?.text = "Sign-in with Facebook";
        fbLoginBtn.delegate = self;
        fbLoginBtn.readPermissions = ["public_profile", "email", "user_friends"];
        NSLog("permissions \(fbLoginBtn.readPermissions)")
        // Configure the sign in object.
        signIn.delegate = self;
        gppSignInBtn.style = kGPPSignInButtonStyleWide;
        gppSignInBtn.titleLabel?.text = "Login with Google+";
        gppSignInBtn.frame = CGRectMake(self.fbLoginBtn.frame.origin.x, self.fbLoginBtn.frame.origin.y + 55, self.fbLoginBtn.frame.size.width, 40.0);
        loginViewTopSpace.constant = -500.0;
        // TODO: Change where the log in button is positioned in your view
       // logInButton.center = self.view.center
       // self.view.addSubview(logInButton)

        // Do any additional setup after loading the view, typically from a nib.
        
        
        /*let logInButton = TWTRLogInButton(logInCompletion:{ (session, error) in
                
                if(session != nil) {
                    
                    print("logged in as \(session!.userName)");
                    
                } else{
                    
                    print("error: \(error!.localizedDescription)");
                    
                }
                
        })
        
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    
    @IBAction func googleLogin(sender: AnyObject) {
        loginMode = SocialLoginTypes.GOOGLEPLUS.rawValue;
        signIn.trySilentAuthentication();
    }
    
    @IBAction func twiitterLogin(sender: AnyObject) {
        Twitter.sharedInstance().logInWithCompletion {(session, error) -> Void in
            
            if(session != nil) {
                self.loginMode = SocialLoginTypes.TWITTER.rawValue;

                print("signed in as \(session!.userName)");
                print("signed in as \(session!.authToken)");
                self.socialAccessToken = (session?.authToken)!;
               // fetchUserDetails()
                self.signIn(self.twitterSignInBtn);
                
            } else{
                
                print("error: \(error!.localizedDescription)");
                
            }
            
        }
     }


    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("User Logged In")
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog("%@",result.grantedPermissions);
            loginMode = SocialLoginTypes.FACEBOOK.rawValue;
            socialAccessToken = result.token.tokenString;
            if result.grantedPermissions.contains("email")
            {
                returnUserData()
            }
        }
    }
    
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,name,email,picture", parameters: nil)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                print("fetched user: \(result)")
                let userName : NSString = result.valueForKey("name") as! NSString
                print("User Name is: \(userName)")
                self.email = result["email"] as! String;
                print("User Email is: \(self.email)")
                self.profileImg = result["picture"]!!["data"]!!["url"] as! String
                self.registerDict.setValue(userName, forKey: "name");
                self.registerDict.setValue(result["id"] as! String, forKey: "fbSocialLoginId")
                self.registerDict.setValue(self.profileImg, forKey: "imageUrl");
                
                self.signIn(self.fbLoginBtn);
            }
        })
    }
    
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        print("User Logged in of google plus \(auth)");
        socialAccessToken = auth.accessToken;
        self.email = auth.userEmail;
        print("signIn of google plus \(signIn.userEmail)");
        print("userData \(auth.userData)");
        let query : GTLQueryPlus = GTLQueryPlus.queryForPeopleGetWithUserId("me") as! GTLQueryPlus;
        let plusService : GTLServicePlus = GTLServicePlus();
        plusService.retryEnabled = true;
        plusService.authorizer = GPPSignIn.sharedInstance().authentication;
        plusService.executeQuery(query) { (ticket, person, error) -> Void in
            print("userData \(person)");
           // self.name = person.displayName as! String;
            self.registerDict.setValue(person.displayName, forKey: "name");
            self.registerDict.setValue(person.identifier, forKey: "googlePlusSocialId");
            if #available(iOS 9.0, *) {
                self.registerDict.setValue((person.image().url) , forKey: "imageUrl")
            } else {
                // Fallback on earlier versions
            };
            self.signIn(self.gppSignInBtn);
            print("self.registerDict \(self.registerDict)");

        }
    }
    @IBAction func loginToApp(sender: AnyObject) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.loginViewTopSpace.constant = 0.0;
            self.loginToAppView.layoutIfNeeded();
        };
    }
    @IBAction func cancelLoginToApp(sender: AnyObject) {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.loginViewTopSpace.constant = -500.0;
            self.loginToAppView.layoutIfNeeded();
        };
    }
    @IBAction func loginByEmail(sender: AnyObject) {
        if(username.text == ""){
            viewControllerUtils.applyShakeValidation(username);
            username.layer.borderColor = UIColor.redColor().colorWithAlphaComponent(0.1).CGColor;
        } else if(password.text == ""){
            viewControllerUtils.applyShakeValidation(password);
            username.layer.borderColor = UIColor.redColor().colorWithAlphaComponent(0.1).CGColor;
        } else{
            self.signIn(sender)
        }
    }
    
    /**
     Handles the Sign In Action    */
    @IBAction func signIn(sender: AnyObject) {
       // viewControllerUtils.showActivityIndicator(loginView)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let emailTxt = (username.text != "") ? username.text!.lowercaseString : email;
        email = username.text!
        let passwordtxt = password.text
        let deviceName = UIDevice.currentDevice().name
        let deviceVersion =  UIDevice.currentDevice().systemVersion
        let deviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
        let platform = UIDevice.currentDevice().systemName
        parametersDict.setValue(emailTxt, forKey: emailKey)
        if(loginMode != SocialLoginTypes.EMAIL.rawValue){
            parametersDict.setValue(socialAccessToken, forKey: socialAccessTokenKey)
        } else {
            parametersDict.setValue(passwordtxt, forKey: passwordKey)
        }
        parametersDict.setValue(passwordtxt, forKey: passwordKey)
        //parametersDict.setValue(stringUtils.hash(passwordtxt), forKey: passwordKey)
        parametersDict.setValue(deviceName, forKey: deviceNameKey)
        parametersDict.setValue(deviceId, forKey: deviceIdKey)
        parametersDict.setValue(deviceVersion, forKey: deviceVersionKey)
        parametersDict.setValue(platform, forKey: platformKey)
        parametersDict.setValue(KEY, forKey: "key")
        parametersDict.setValue("1.0", forKey: "appVersion")
        parametersDict.setValue(loginMode, forKey: "loginMode")
        parametersDict.setValue(NSUserDefaults.standardUserDefaults().objectForKey("device-token"), forKey: "deviceToken")
        print(parametersDict)
        currentFetch = loginRequest;
        let jsonData : NSData = try! NSJSONSerialization.dataWithJSONObject(parametersDict, options: [])
         urlRequest = NSMutableURLRequest(URL: NSURL(string: HOST_IP + authURL)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = jsonData
        currentFetch = loginRequest;
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        networkManager.sendRequest(urlRequest, jsonName: loginRequest)
    }
    
    
    func didReceiveResponse(responseDict : NSDictionary){

        if(currentFetch == loginRequest){
            if(responseDict["access_token"] != nil){
                self.performSegueWithIdentifier("followViewControllerSegue", sender: self);
                NSUserDefaults.standardUserDefaults().setObject(responseDict["access_token"] as! String, forKey: accessTokenKey)
                NSUserDefaults.standardUserDefaults().setObject(loginMode, forKey: "loginMode")
                NSUserDefaults.standardUserDefaults().synchronize()
                
            } else if(responseDict["message"] as! String == "New User") {
                self.performSegueWithIdentifier("registerViewControllerSegue", sender: self);
            } else  {
                NSLog("auth failure");
                viewControllerUtils.showAlertView("Alert", message: responseDict["message"] as! String);
            }
 
        }
    }
    
    func requestFailedWithError(error : NSError){
        print(error)

        
    }
    
    func didReceiveResponseCode(response : NSHTTPURLResponse){
        print(response)

        
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "registerViewControllerSegue"){
            let registerVC : RegisterViewController = segue.destinationViewController as! RegisterViewController;
            registerDict.setValue(email, forKey: "email")
            registerVC.authParamDict = parametersDict;
            registerVC.registerParamDict = registerDict;
        }
    }
    
    func checkForActiveSession () -> Bool{
        return true
    }
}

