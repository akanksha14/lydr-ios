//
//  ViewControllerUtil.swift
//  Lydr
//
//  Created by Akanksha Sharma on 19/11/15.
//  Copyright © 2015 akanksha. All rights reserved.
//

import Foundation
class ViewControllerUtils : UIViewController  {
    func applyShakeValidation(view : UIView){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(view.center.x - 5, view.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(view.center.x + 5, view.center.y))
        view.layer.addAnimation(animation, forKey: "position")
        view.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.1);
    }
    
    func clearValidation (validationViews : NSMutableArray){
        for(var i = 0; i < validationViews.count ; i++){
            (validationViews.objectAtIndex(i) as! UIView).backgroundColor = UIColor.clearColor();
        }
    }
    
    func showAlertView(title : String , message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        UIApplication.sharedApplication().keyWindow?.rootViewController!.presentViewController(alert, animated: true, completion: nil)
    }
}
