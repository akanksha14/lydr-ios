//
//  FollowLeaderViewController.swift
//  Lydr
//
//  Created by Akanksha Sharma on 07/11/15.
//  Copyright © 2015 akanksha. All rights reserved.
//

import Foundation

class FollowLeaderViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    
    
    @IBOutlet weak var leadersCollectionView: UICollectionView!
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 6;
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell : SuggestedLeaderCell = leadersCollectionView.dequeueReusableCellWithReuseIdentifier("leaderCell", forIndexPath: indexPath) as! SuggestedLeaderCell;
        
        return cell;
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1;
    }
    
}
