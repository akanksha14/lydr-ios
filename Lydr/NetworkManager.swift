//
//  NetworkService.swift
//  InstaPat
//
//  Created by Akanksha Sharma on 4/15/15.
//  Copyright (c) 2015 kwench. All rights reserved.
//

import Foundation
import UIKit


class NetworkManager : NetworkCommunicatorDelegate{
    
    var delegate: NetworkManagerDelegate?
    var communicator : NetworkCommunicator?
    var objectBuilder : ObjectBuilder = ObjectBuilder()
    /**
    Called when the response is received.
    */
    func didReceiveResponse(objectNotation : NSData, jsonName : NSString){
        var localErr : NSError?
        let responseDict : NSDictionary = objectBuilder.objectsFromJSON(objectNotation, jsonName: jsonName, error: &localErr)
        print("did receive response begins \(responseDict)")
        if !(localErr != nil){
            self.delegate?.didReceiveResponse(responseDict)
        } else {
           self.delegate?.requestFailedWithError(localErr!)
        }
    }
    
    /**
    Called when the error is received.
    */
    func requestFailedWithError(error : NSError){
        self.delegate?.requestFailedWithError(error)
    }
    
    /**
    Handles the response according to the response code
    */
    func didReceiveResponseCode(response : NSHTTPURLResponse){
        self.delegate?.didReceiveResponseCode(response)
    }
    
    
    func sendRequest(request : NSMutableURLRequest,jsonName : NSString){
        print(request)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        communicator?.fetchDataRequest(request,jsonName: jsonName)
    }
    
}

protocol NetworkManagerDelegate{
    /**
    Called when the response is received.
    */
    func didReceiveResponse(groupDic : NSDictionary)
    
    /**
    Called when the error is received.
    */
    func requestFailedWithError(error : NSError)
    
    /**
    Handles the response according to the response code
    */
    func didReceiveResponseCode(response : NSHTTPURLResponse)

}