//
//  StringUtil.m
//  InstaPat
//
//  Created by Akanksha Sharma on 3/18/15.
//  Copyright (c) 2015 com.charan. All rights reserved.
//

#import "StringUtils.h"
#import <CommonCrypto/CommonHMAC.h>


@implementation StringUtils

-(NSString *)convertBrToNewlines:(NSString *)str{
    return  [str stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
}

-(NSString *)hash:(NSString *)str{
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    const char *cStr = [str UTF8String];
    CC_SHA1(cStr, strlen(cStr), result); //Now result contains the hash
    
    //Wrap the result in a NSData object
    NSData *pwHashData = [[NSData alloc] initWithBytes:result length: sizeof result];
    //And take Base64 of that
    NSString *base64str = [pwHashData base64Encoding];
    return base64str;
}

@end
