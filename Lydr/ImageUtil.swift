//
//  ImageUtil.swift
//  InstaPat
//
//  Created by Akanksha Sharma on 5/13/15.
//  Copyright (c) 2015 kwench. All rights reserved.
//

import Foundation
import UIKit

func getImageFromURL(var path :String)-> UIImage{
    /*if(path.hasSuffix(".jpg")){
        path = path.stringByReplacingOccurrencesOfString(".jpg", withString: ".png", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }*/
    let url = NSURL(string : path)
    if(url != nil){
        let data = NSData(contentsOfURL: url!)
        if(data != nil){
            return UIImage(data: data!)!
        } else {
            return UIImage(named: "default-user.png")!
        }
    } else {
        return UIImage(named: "default-user.png")!
    }

}


